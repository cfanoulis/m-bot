// Define JSON File
 var fs = require("fs");
 console.log("\n *STARTING PARSE* \n");
// Get content from file
 var contents = fs.readFileSync("config.json");
// Define to JSON type
 var jsonContent = JSON.parse(contents);
 console.log("\n *PARSE ENDED* \n");

const Discord = require("discord.js");

var TOKEN = jsonContent.token
var PREFIX = jsonContent.prefix

const ServerLink = jsonContent.server
const BetaLink = jsonContent.betaform

const hook = new Discord.WebhookClient(jsonContent.webhookID, jsonContent.webhookSecret);

const bot = new Discord.Client();

const usercmds = "null"
const donatorcmds = "null"
const modcmds = "clear,cleanup,kick,ban,timeout"

const helpembed = new Discord.RichEmbed()
  .setTitle("MirrorBot")
  .setFooter("MirrorBot! · Created for you!")
  .setDescription("MirrorBot is an exclusive bot, only for our Discord server, there are no contributors! (Eddy, are you sure?)")
  .addField("User commands", usercmds)
  .addField("Donator commands", donatorcmds)
  .addField("Admin/moderator commands", modcmds)
  .setColor(13,255,161)

bot.on("ready", function() {
    bot.user.setPresence({ game: { name: jsonContent.game }, status: jsonContent.status});
    console.log("The Mirror is now up!");
    hook.send("Report: The Bot is up!");
});

bot.on("message", function(message) {
    if (message.author.equals(bot.user)) return;

    if (!message.content.startsWith(PREFIX)) return;

    var args = message.content.substring(PREFIX.length).split(" ");

    switch (args[0].toLowerCase()) {
        case "help":
            message.channel.sendEmbed(helpembed);
            break;
        case "echo":
          message.channel.send("Peng!")
            break;
        case "destroy":
          if (message.author.id !== jsonContent.adminID) {
            message.channel.send("Sorry, but you do not have the right to execute this command!")
          } else {
            message.channel.send("Beep-woop! The bot will shut down in 10 seconds!")
            hook.send("Bot destroy initiated!")
            setTimeout(function() {
                message.channel.send("The bot is destroyed. Au Revoir!")
                hook.send("Bot DOStroyed")
                bot.destroy();
            }, 10000);
          }
          break;
        case "leave":
          if (message.author.id !== jsonContent.adminID) {
            message.channel.send("You may not kick me out of this guild!")
            message.channel.send({files: ['https://pbs.twimg.com/media/DcnjhuLVMAAj_8H.jpg']})
          } else {
              message.channel.send("I have left this guild. Should you want to re-invite me, just click here --> https://mirroredbot.page.link/invite")
              message.guild.leave()

          }
          break;
        default: // Must be the latest line in the switch method!
            message.channel.send("Sorry, but this command is not recognised!");
    }
});

bot.on("disconnected", function() { // erase that line if need (beta)
    hook.send("Bot has been disconnected");
    console.log("The Mirror has fallen! Check the logs for errors!");
});

//New member notification, comment if it doesn't work
  bot.on('guildMemberAdd', member => {
    const welcomechannel = member.guild.channels.find('name', 'member-log');
    if (!channel) return;
    channel.send(`Welcome to the server, ${member}`);
});

bot.login(TOKEN);
